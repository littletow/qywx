# 企业微信SDK库 For Golang

#### 介绍
企业微信SDK库 [企业微信API文档地址](https://open.work.weixin.qq.com/api/doc/90000/90003/90556)

在企业微信中，每个应用的secret都不一样，token是每个业务方法使用的通用票据，因此，我们需要单独的进行存储。

目录定义：

services：存放业务类

- services/caches:存放缓存工具
- services/commons:存放通用的错误异常、配置项、消息加解密和验证、企业微信API的IP列表、HTTP调用、素材管理等
- services/tokens:存放token获取方法 
- services/callback: 企业微信回调配置管理,存放企业微信XML消息加密算法工具
- services/contacts:存放通讯录管理
- services/apps:存放自建应用管理
- services/msgs:存放消息推送管理
- services/oas:存放OA管理
- services/utils: 存放工具，缓存管理，加解密算法


#### 使用说明

```
import "gitee.com/littletow/qywx"
```

1. 创建缓存实例
```golang
// 这里以内存缓存为例
cache := qywx.NewCache(qywx.MEMORY_CACHE,caches.RedisOpts{},"")
```
2. 创建企业微信实例
```golang
qywxClient := qywx.NewQywx(cache)
```
3. 调用各个功能模块
```golang
// 调用通讯录管理模块
corpid := "企业ID"
agentid := commons.ContactsID
secret := "企业通信录密钥" 
manager := qywxClient.GetContactManager(corpid,agentid,secret)
// 调用通讯录管理的各个功能,例如创建成员
req := contacts.CreateUserReq{
    // 属性赋值
    ...
}
// 调用CreateUser，返回结果，根据err判断是否成功
manager.CreateUser(req)

```

4. 回调服务管理调用

在集成企业微信与内部系统时，我们往往需要搭建一个回调服务。回调服务，可以实现：

自定义丰富的服务行为。比如，用户向应用发消息时，识别消息关键词，回复不同的消息内容；用户点击应用菜单时，转化为指令，执行自动化任务。
可以及时获取到状态变化。比如，通讯录发生变化时，不需要定时去拉取通讯录对比，而是实时地获取到变化的通讯录结点，进行同步。

```golang
// 我们配置回调
receiver_id := "" // 企业ID,或应用suitid
token := "" // 配置回调时生成
encodingAESKey := "" // 加密消息的密钥

cbManager := qywx.GetCallbackManager(receiver_id,token,encodingAESKey)
// 获取回调实例后，就可以调用方法，例如CallbackVerifyURL，验证URL，参数可以从URL中获取
req := callback.CallbackVerifyURLReq{
    // 属性设置
    ...
}

resp,err:=cbManager.CallbackVerifyURL(req)

// 可以将结果直接返回企业微信平台
```

它还有很多方法的方法，例如加密消息体，解密消息体。

有公用的消息结构体，只有组织为对应的结构体，就可以进行消息回复。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

