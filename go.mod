module gitee.com/littletow/qywx

go 1.15

require (
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/gomodule/redigo v1.8.4
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
)
