package caches

import "time"

// Cache 缓存接口，存放，获取，检测是否存在Key，删除
type Caches interface {
	Get(key string) interface{}
	Set(key string, val interface{}, timeout time.Duration) error
	IsExist(key string) bool
	Delete(key string) error
}
