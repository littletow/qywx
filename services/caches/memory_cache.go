package caches

import (
	"sync"
	"time"
)

//Memory 使用内存存放
type Memory struct {
	sync.Mutex

	data map[string]*data
}

// data 数据，包括值和失效时间
type data struct {
	Data    interface{}
	Expired time.Time
}

//NewMemory 创建一个内存实例
func NewMemory() *Memory {
	return &Memory{
		data: make(map[string]*data),
	}
}

//Get 返回缓存值
func (mem *Memory) Get(key string) interface{} {
	if ret, ok := mem.data[key]; ok {
		if ret.Expired.Before(time.Now()) {
			mem.deleteKey(key)
			return nil
		}
		return ret.Data
	}
	return nil
}

// IsExist 检查键值是否存在
func (mem *Memory) IsExist(key string) bool {
	if ret, ok := mem.data[key]; ok {
		if ret.Expired.Before(time.Now()) {
			mem.deleteKey(key)
			return false
		}
		return true
	}
	return false
}

//Set 存储键值
func (mem *Memory) Set(key string, val interface{}, timeout time.Duration) (err error) {
	mem.Lock()
	defer mem.Unlock()

	mem.data[key] = &data{
		Data:    val,
		Expired: time.Now().Add(timeout),
	}
	return nil
}

//Delete 删除键值
func (mem *Memory) Delete(key string) error {
	mem.deleteKey(key)
	return nil
}

// deleteKey 删除键
func (mem *Memory) deleteKey(key string) {
	mem.Lock()
	defer mem.Unlock()
	delete(mem.data, key)
}
