package commons

// 企业微信中一些应用没有agentid，这里设置常量
const (
	ContactsID = "qywx001" // 企业微信通讯录
)

// QywxConfig 企业微信配置
type QywxConfig struct {
	CorpID  string // corpid 企业微信ID
	AgentID string // 应用ID
	Secret  string // 应用秘钥

}
