package commons

import (
	"encoding/json"
	"fmt"
)

// 参考https://work.weixin.qq.com/api/doc/90000/90135/91054
const (
	UploadMediaURL  string = "https://qyapi.weixin.qq.com/cgi-bin/media/upload"
	UploadImgURL    string = "https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg"
	GetMediaURL     string = "https://qyapi.weixin.qq.com/cgi-bin/media/get"
	GetHighVoiceURL string = "https://qyapi.weixin.qq.com/cgi-bin/media/get/jssdk"
)

// UploadMediaResp 上传临时素材响应
type UploadMediaResp struct {
	CommonError
	Type      string `json:"type"`
	MediaID   string `json:"media_id"`
	CreatedAt string `json:"created_at"`
}

// UploadImgResp 上传图片响应
type UploadImgResp struct {
	CommonError
	URL string `jsong:"url"`
}

// UploadMedia 上传临时素材
// https://work.weixin.qq.com/api/doc/90000/90135/90253
func UploadMedia(accessToken string, mediaType string, filename string) (*UploadMediaResp, error) {
	url := fmt.Sprintf("%s?access_token=%s&type=%s", UploadMediaURL, accessToken, mediaType)
	body, err := PostFile("media", filename, url)
	if err != nil {
		return nil, err
	}

	var resp UploadMediaResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("UploadMedia error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}

	return &resp, nil

}

// UploadImg 上传图片
// https://work.weixin.qq.com/api/doc/90000/90135/90256
func UploadImg(accessToken string, filename string) (*UploadImgResp, error) {
	url := fmt.Sprintf("%s?access_token=%s", UploadImgURL, accessToken)

	body, err := PostFile("img", filename, url)
	if err != nil {
		return nil, err
	}

	var resp UploadImgResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("UploadImg error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}

	return &resp, nil

}

// GetMedia 获取临时素材
// https://work.weixin.qq.com/api/doc/90000/90135/90254
func GetMedia(accessToken string, mediaid string) ([]byte, error) {
	url := fmt.Sprintf("%s?access_token=%s&media_id=%s", GetMediaURL, accessToken, mediaid)
	body, err := HTTPGet(url)

	// 网络错误
	if err != nil {
		return nil, err
	}

	var resp CommonError
	err = json.Unmarshal(body, &resp)
	// 是文件，不能解析
	if err != nil {
		return body, nil
	} else {
		err = fmt.Errorf("GetMedia error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}

}

// GetHighVoice 获取高清语音素材
// https://work.weixin.qq.com/api/doc/90000/90135/90255
func GetHighVoice(accessToken string, mediaid string) ([]byte, error) {
	url := fmt.Sprintf("%s?access_token=%s&media_id=%s", GetHighVoiceURL, accessToken, mediaid)
	body, err := HTTPGet(url)

	// 网络错误
	if err != nil {
		return nil, err
	}

	var resp CommonError
	err = json.Unmarshal(body, &resp)
	// 是文件，不能解析
	if err != nil {
		return body, nil
	} else {
		err = fmt.Errorf("GetHighVoice error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}

}
