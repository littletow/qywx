package apps

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitee.com/littletow/qywx/services/commons"
)

// 自建应用的方法实现
const (
	GetAgentDetailURL string = "https://qyapi.weixin.qq.com/cgi-bin/agent/get"
	GetAgentListURL   string = "https://qyapi.weixin.qq.com/cgi-bin/agent/list"
	SetAgentURL       string = "https://qyapi.weixin.qq.com/cgi-bin/agent/set"
)

// AllowUserinfos 允许用户信息列表
type AllowUserinfos struct {
	Users []struct {
		UserID string `json:"userid"`
	} `json:"user"`
}

// AllowPartys 允许部门列表
type AllowPartys struct {
	PartyID []int `json:"partyid"`
}

// AllowTags 允许标签列表
type AllowTags struct {
	TagID []int `json:"tagid"`
}

// GetAgentDetailResp 获取指定的应用详情响应
type GetAgentDetailResp struct {
	commons.CommonError
	AgentID            string                   `json:"agentid"`         // 企业应用id
	Name               string                   `json:"name"`            // 企业应用名称
	SquareLogoURL      string                   `json:"square_logo_url"` // 企业应用方形头像
	Description        string                   `json:"description"`     // 企业应用详情
	AllowUserinfos     `json:"allow_userinfos"` // 企业应用可见范围（人员），其中包括userid
	AllowPartys        `json:"allow_partys"`    // 企业应用可见范围（部门）
	AllowTags          `json:"allow_tags"`      // 企业应用可见范围（标签）
	Close              int                      `json:"close"`                // 企业应用是否被停用
	RedirectDomain     string                   `json:"redirect_domain"`      // 企业应用可信域名
	ReportLocationFlag int                      `json:"report_location_flag"` // 企业应用是否打开地理位置上报 0：不上报；1：进入会话上报；
	IsReportenter      int                      `json:"isreportenter"`        // 是否上报用户进入应用事件。0：不接收；1：接收
	HomeURL            string                   `json:"home_url"`             // 应用主页url
}

// GetAgentListResp 获取access_token对应的应用列表响应
type GetAgentListResp struct {
	commons.CommonError
	AgentList []AgentItem `json:"agentlist"` // 当前凭证可访问的应用列表
}

// AgentItem 应用项目
type AgentItem struct {
	AgentID       int    `json:"agentid"`         // 企业应用id
	Name          string `json:"name"`            // 企业应用名称
	SquareLogoURL string `json:"square_logo_url"` // 企业应用方形头像
}

// SetAgentReq 设置应用请求
type SetAgentReq struct {
	AgentID            string `json:"agentid"`                        // 企业应用id
	ReportLocationFlag int    `json:"report_location_flag,omitempty"` // 企业应用是否打开地理位置上报 0：不上报；1：进入会话上报；
	Name               string `json:"name,omitempty"`                 // 企业应用名称
	Description        string `json:"description,omitempty"`          // 企业应用详情
	HomeURL            string `json:"home_url,omitempty"`             // 应用主页url
	RedirectDomain     string `json:"redirect_domain,omitempty"`      // 企业应用可信域名
	IsReportenter      int    `json:"isreportenter,omitempty"`        // 是否上报用户进入应用事件。0：不接收；1：接收
	LogoMediaID        string `json:"logo_mediaid,omitempty"`         // 企业应用头像的mediaid，通过素材管理接口上传图片获得mediaid，上传后会自动裁剪成方形和圆形两个头像
}

// GetAgentDetail 获取指定的应用详情
// 参考 https://open.work.weixin.qq.com/api/doc/90000/90135/90227
func (manager *CustomAppManager) GetAgentDetail(agentid string) (*GetAgentDetailResp, error) {
	if agentid == "" {
		return nil, errors.New(commons.ErrParamInValid)
	}
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s?access_token=%s&agentid=%s", GetAgentDetailURL, token, agentid)

	var body []byte
	body, err = commons.HTTPGet(url)
	if err != nil {
		return nil, err
	}
	var resp GetAgentDetailResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("GetAgentDetail error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil
}

// GetAgentList 获取access_token对应的应用列表
// 参考 https://open.work.weixin.qq.com/api/doc/90000/90135/90227
func (manager *CustomAppManager) GetAgentList() (*GetAgentListResp, error) {

	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s?access_token=%s", GetAgentDetailURL, token)

	var body []byte
	body, err = commons.HTTPGet(url)
	if err != nil {
		return nil, err
	}
	var resp GetAgentListResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("GetAgentList error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil
}

// SetAgent 设置自建应用
// 参考 https://open.work.weixin.qq.com/api/doc/90000/90135/90228
func (manager *CustomAppManager) SetAgent(req SetAgentReq) (*commons.CommonError, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if req.AgentID == "" {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s", SetAgentURL, token)
	// 构造Body
	var body []byte
	body, err = commons.PostJSON(url, req)
	if err != nil {
		return nil, err
	}
	var resp commons.CommonError
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("SetAgent error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil
}
