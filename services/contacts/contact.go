package contacts

// 通讯录管理
import (
	"gitee.com/littletow/qywx/services/commons"
	"gitee.com/littletow/qywx/services/tokens"
)

// ContactManager 通讯录管理服务
type ContactManager struct {
	tokenStore *tokens.AccessTokenStore
	config     *commons.QywxConfig
}

// NewContactManager 返回一个manager的实例
func NewContactManager(corpid string, agentid string, secret string, tokenStore *tokens.AccessTokenStore) *ContactManager {
	config := commons.QywxConfig{
		CorpID:  corpid,
		AgentID: agentid,
		Secret:  secret,
	}
	// 不在这里获取token，是防止token失效问题，每次调用都从tokenStore中获取。例如：（实例化在10:00，停止2个小时，再调用方法，就会出现问题）
	return &ContactManager{tokenStore: tokenStore, config: &config}
}

// getToken 获取token
func (manager *ContactManager) getToken() (string, error) {
	req := tokens.AccessTokenReq{
		CorpID:  manager.config.CorpID,
		AgentID: manager.config.AgentID,
		Secret:  manager.config.Secret,
	}
	// 获取token
	token, err := manager.tokenStore.GetToken(manager.config.AgentID, req)
	if err != nil {
		return "", err
	}
	return token, nil
}
