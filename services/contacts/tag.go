package contacts

// 标签接口
import (
	"encoding/json"
	"errors"
	"fmt"

	"gitee.com/littletow/qywx/services/commons"
)

const (
	// 标签URL
	CreateTagURL  string = "https://qyapi.weixin.qq.com/cgi-bin/tag/create"
	UpdateTagURL  string = "https://qyapi.weixin.qq.com/cgi-bin/tag/update"
	DeleteTagURL  string = "https://qyapi.weixin.qq.com/cgi-bin/tag/delete"
	GetTagUserURL string = "https://qyapi.weixin.qq.com/cgi-bin/tag/get"
	AddTagUserURL string = "https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers"
	DelTagUserURL string = "https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers"
	GetTagListURL string = "https://qyapi.weixin.qq.com/cgi-bin/tag/list"
)

// CreateTagReq 创建标签请求
type CreateTagReq struct {
	TagName string `json:"tagname"`
	TagID   int    `json:"tagid,omitempty"`
}

// CreateTagResp 创建标签响应
type CreateTagResp struct {
	commons.CommonError
	TagID int `json:"tagid"`
}

// UpdateTagReq 更改标签名称请求
type UpdateTagReq struct {
	TagName string `json:"tagname"`
	TagID   int    `json:"tagid"`
}

// GetTagUserResp 获取标签成员响应
type GetTagUserResp struct {
	commons.CommonError
	TagName  string `json:"tagname"`
	UserList []struct {
		UserID string `json:"userid"`
		Name   string `json:"name"`
	} `json:"userlist"`
	PartyList []int `json:"partylist"` // 标签中包含的部门ID列表
}

// AddTagUserReq 创建标签用户请求
type AddTagUserReq struct {
	TagID     int      `json:"tagid"`
	UserList  []string `json:"userlist,omitempty"`
	PartyList []int    `json:"partylist,omitempty"`
}

// AddTagUserReq 创建标签用户响应
type AddTagUserResp struct {
	commons.CommonError
	InvalidList  []string `json:"invalidlist,omitempty"`  // 非法的成员帐号列表
	InvaildParty []int    `json:"invalidparty,omitempty"` // 非法的部门id列表
}

// DelTagUserReq 删除标签用户请求
type DelTagUserReq struct {
	TagID     int      `json:"tagid"`
	UserList  []string `json:"userlist,omitempty"`
	PartyList []int    `json:"partylist,omitempty"`
}

// DelTagUserResp 删除标签用户响应
type DelTagUserResp struct {
	commons.CommonError
	InvalidList  []string `json:"invalidlist,omitempty"`  // 非法的成员帐号列表
	InvaildParty []int    `json:"invalidparty,omitempty"` // 非法的部门id列表
}

// GetTagListResp 获取标签列表响应
type GetTagListResp struct {
	commons.CommonError
	TagList []struct {
		TagID   int    `jsong:"tagid"`
		TagName string `jsong:"tagname"`
	} `json:"taglist"`
}

// CreateTag 创建标签
// https://work.weixin.qq.com/api/doc/90000/90135/90210
func (manager *ContactManager) CreateTag(req CreateTagReq) (*CreateTagResp, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if req.TagName == "" {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s", CreateTagURL, token)
	// 构造Body
	var body []byte
	body, err = commons.PostJSON(url, req)
	if err != nil {
		return nil, err
	}
	var resp CreateTagResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("CreateTag error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}

// UpdateTag 更改标签名称
// https://work.weixin.qq.com/api/doc/90000/90135/90211
func (manager *ContactManager) UpdateTag(req UpdateTagReq) (*commons.CommonError, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if req.TagName == "" || req.TagID < 1 {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s", UpdateTagURL, token)
	// 构造Body
	var body []byte
	body, err = commons.PostJSON(url, req)
	if err != nil {
		return nil, err
	}
	var resp commons.CommonError
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("UpdateTag error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}

// DeleteTag 删除标签
// https://work.weixin.qq.com/api/doc/90000/90135/90212
func (manager *ContactManager) DeleteTag(tagid int) (*commons.CommonError, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if tagid < 1 {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s&tagid=%d", DeleteTagURL, token, tagid)
	// 构造Body
	var body []byte
	body, err = commons.HTTPGet(url)
	if err != nil {
		return nil, err
	}
	var resp commons.CommonError
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("DeleteTag error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}

// GetTagUser 获取标签成员
// https://work.weixin.qq.com/api/doc/90000/90135/90213
func (manager *ContactManager) GetTagUser(tagid int) (*GetTagUserResp, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if tagid < 1 {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s&tagid=%d", GetTagUserURL, token, tagid)
	// 构造Body
	var body []byte
	body, err = commons.HTTPGet(url)
	if err != nil {
		return nil, err
	}
	var resp GetTagUserResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("GetTagUser error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}

// AddTagUser 增加标签成员
// https://work.weixin.qq.com/api/doc/90000/90135/90214
func (manager *ContactManager) AddTagUser(req AddTagUserReq) (*AddTagUserResp, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if req.TagID < 1 {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s", AddTagUserURL, token)
	// 构造Body
	var body []byte
	body, err = commons.PostJSON(url, req)
	if err != nil {
		return nil, err
	}
	var resp AddTagUserResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("AddTagUser error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}

// DelTagUser 删除标签成员
// https://work.weixin.qq.com/api/doc/90000/90135/90215
func (manager *ContactManager) DelTagUser(req DelTagUserReq) (*DelTagUserResp, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}
	if req.TagID < 1 {
		return nil, errors.New(commons.ErrParamInValid)
	}

	url := fmt.Sprintf("%s?access_token=%s", DelTagUserURL, token)
	// 构造Body
	var body []byte
	body, err = commons.PostJSON(url, req)
	if err != nil {
		return nil, err
	}
	var resp DelTagUserResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("DelTagUser error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}

// GetTagList 获取标签列表
// https://work.weixin.qq.com/api/doc/90000/90135/90216
func (manager *ContactManager) GetTagList() (*GetTagListResp, error) {
	token, err := manager.getToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s?access_token=%s", GetTagListURL, token)
	// 构造Body
	var body []byte
	body, err = commons.HTTPGet(url)
	if err != nil {
		return nil, err
	}
	var resp GetTagListResp
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}
	// 出错返回码，为0表示成功，非0表示调用失败
	if resp.ErrCode != 0 {
		err = fmt.Errorf("GetTagList error : errcode=%v , errormsg=%v", resp.ErrCode, resp.ErrMsg)
		return nil, err
	}
	return &resp, nil

}
